/**
 * Import Dependencies
 */
import jwt from 'jsonwebtoken';
import { config as dotenv } from 'dotenv';
import { NextFunction, Request, Response } from 'express';
import { UserModel } from '../api/user_manage/model/user.model';
import { User } from '../api/user_manage/model/user';

/**
 * @class json web token management class
 * and its verification of the accounts
 */
export class AuthJsonWebToken {

    /**
     * @public
     * process to verify the token every time a request is requested
     * @param req : request HTTP
     * @param res : response HTTP
     * @param next : continuation process to the function of a controller
     * @returns HTTP response or none, as it continued with the HTTP request
     */
    public static async verifyToken(req: Request, res: Response, next: NextFunction): Promise<Response | void> {
        dotenv();
        try {
            let token: any = req.headers["x-access-token"];
            if (!token) {
                // not token received
                console.log('\x1b[32m', "no token received!!", '\x1b[0m');
                return res.status(403).json({ message: `Not token provided` });
            } else {
                // i have a token
                let decoded: any = jwt.verify(token, `${process.env.KEY_SECRET}`);
                let userModel: UserModel = new UserModel();
                let userData: User | undefined = await userModel.findById(decoded.id);
                if (!userData) {
                    // if not user exists
                    return res.status(404).json({ message: `not user found` });
                } else {
                    // if user exists
                    console.log('\x1b[32m', "authenticated success", '\x1b[0m');
                }
            }
            next();
        } catch (error) {
            console.log("Error is method verifyToken()", error);
            return res.status(203).json({ message: `Unauthorized` });
        }
    }

    /**
     * @public
     * Gets a token generated for a user who logged in
     * and this token has a delay of 24 hours or 86400 seconds
     * @param accountUserCI : identifier code of the user who owns the token
     * @returns generated token
     */
    public static generateToken(idUser: string): string {
        dotenv();
        return jwt.sign({ id: idUser }, `${process.env.KEY_SECRET}`, { expiresIn: 86400 });
    }
}