/**
 * Import Models
 */

import { Device } from "../model/device";
import { DeviceModel } from "../model/device.model";


/**
 * @class of business logic implementation for manage services users.
 */
export class DeviceService {

    private deviceModel: DeviceModel;

    /**
     * @public
     * @constructor
     * initializes the instances of the models dependent on this service.
     */
    public constructor() {
        this.deviceModel = new DeviceModel();
    }

    /**
     * get list device saved information
     * @returns devices saved
     */
    public findAll = async (): Promise<any> => {
        try {
            let listHistorial: Array<Device> = await this.deviceModel.findAll();
            return {
                status: 200,
                msg: "get list success",
                data: listHistorial
            }
        } catch (error) {
            console.log("Error in DeviceService findAll()", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }
}