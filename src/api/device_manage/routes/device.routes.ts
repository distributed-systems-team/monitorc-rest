/**
 * Import Libraries
 */
import { Router } from 'express';

/**
 * Import Files
 */
import { AuthJsonWebToken } from '../../../middlewares/auth';

/**
 * Import Controllers
 */
import { DeviceController } from '../controller/device.controller';

// initialize Router
const routes = Router();
// initialize controllers
const deviceController: DeviceController = new DeviceController();

// routes http here

/**
 * @swagger
 * /device_management/device_manage/all :
 *  post:
 *      summary: This performs the process to get list devices
 *      requestBody:
 *              content:
 *                  application/json:
 *      responses:
 *          200:
 *              description: the login verification returns
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *                              data:
 *                                  type: object
 *                                  description: return list user
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 */
routes.get('/all', deviceController.findAll);

export default routes;