/**
 * Import Components
 */
import { Connection } from "../../../config/database/connection";
import { Device } from "./device";

/**
 * @class model's device
 */
export class DeviceModel {

    /**
     * Attributes
     */
    public connection: Connection;

    /**
     * @public
     * @constructor
     * Obtain an instance of connection to database
     */
    public constructor() {
        this.connection = Connection.getInstance();
    }

    /**
     * @public
     * find all devices
     * @returns devices data else undefined
     */
    public findAll = async (): Promise<Array<Device>> => {
        try {
            let devices = await this.connection.executeQuerySQL(
                `SELECT id_device, date_registered, temperature, humidity, to_char(last_session,'Day DD Mon YYYY HH24:MI:SS') as last_session, status
                FROM device
                ORDER BY last_session desc;`);
            return devices.rows;
        } catch (error) {
            console.error("Error in DeviceModel findAll(): ", error);
            return [];
        }
    }
}