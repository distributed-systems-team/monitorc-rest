export interface Device {
    id_device: string,
    date_registered: string,
    temperature: number,
    humidity: number,
    last_session: string,
    status: boolean
};