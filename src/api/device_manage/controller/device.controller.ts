/**
 * Import Dependencies
 */

/**
 * Import Services
 */
import { Request, Response } from 'express';
import { DeviceService } from '../service/device.service';

/**
 * @class controller device, for the implementation of HTTP requests
 */
export class DeviceController {

    /**
     * Attributes
     */
    private deviceService: DeviceService;

    /**
     * @constructor
     * Initialize user service for implement business logic
     */
    public constructor() {
        this.deviceService = new DeviceService();
    }

    /**
     * get devices saved into database
     * @returns devices
     */
    public findAll = async (requests: Request, response: Response): Promise<Response> => {
        let responseDevice: any = await this.deviceService.findAll();
        return response.status(responseDevice.status).json(responseDevice);
    }
}