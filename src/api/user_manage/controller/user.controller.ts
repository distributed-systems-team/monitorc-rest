/**
 * Import Dependencies
 */
import { Request, Response } from "express";

/**
 * Import Services
 */
import { UserService } from '../service/user.service';

/**
 * @class controller user, for the implementation of HTTP requests
 */
export class UserController {

    /**
     * Attributes
     */
    private userService: UserService;

    /**
     * @constructor
     * Initialize user service for implement business logic
     */
    public constructor() {
        this.userService = new UserService();
    }

    /**
     * This method returns the response of whether the login is accepted or not.
     * @param request : request HTTP
     * @param response : response HTTP
     * @returns a json with estate http, message and data
     */
    public signIn = async (request: Request, response: Response): Promise<Response> => {
        try {
            let { email, password } = request.body;
            let resultSession = await this.userService.authSessionUser(email, password);
            return response.status(resultSession.status).json(resultSession);
        } catch (error) {
            console.log("Error in signIn user controller", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            });
        }
    }

    /**
     * register new account
     * @param request : request HTTP
     * @param response : response HTTP
     * @returns json response to client
     */
    public signUp = async (request: Request, response: Response): Promise<Response> => {
        try {
            let { nickname, email, password } = request.body;
            let responseRegister = await this.userService.registerNewUserAccount(
                { id: -1, nickname, email, password });
            return response.status(responseRegister.status).json(responseRegister);
        } catch (error) {
            console.log("Error in controller register", error);
            return response.status(500).json({
                status: 500,
                msg: "Error internal server"
            });
        }
    }

    /**
     * get list users
     * @param request : HTTP request
     * @param response : HTTP response
     * @returns list pollster
     */
    public getListUsers = async (request: Request, response: Response): Promise<Response> => {
        let responseListUsers = await this.userService.getListUserAccount();
        return response.status(responseListUsers.status).json(responseListUsers);
    }
}