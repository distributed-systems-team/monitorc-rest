/**
 * Import Libraries
 */
import { Router } from 'express';

/**
 * Import Files
 */
import { AuthJsonWebToken } from '../../../middlewares/auth';

/**
 * Import Controllers
 */
import { UserController } from '../controller/user.controller';

// initialize Router
const routes = Router();
// initialize controllers
const userController: UserController = new UserController();

// routes http here

/**
 * @swagger
 * /user_management/user_manage/sign_in :
 *  post:
 *      summary: This performs the process of beginning session (login) for the users
 *      requestBody:
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              email:
 *                                  type: string
 *                              password:
 *                                  type: string
 *      responses:
 *          200:
 *              description: the login verification returns
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *                              jwt?:
 *                                  type: object
 *                                  description: return jwt if is login success; but another, no returns jwt
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 */
routes.post('/sign_in', userController.signIn);

/**
 * @swagger
 * /user_management/user_manage/list_user :
 *  post:
 *      summary: This performs the process to get list account users [AuthJsonWebToken]
 *      requestBody:
 *              content:
 *                  application/json:
 *      responses:
 *          200:
 *              description: the login verification returns
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *                              data:
 *                                  type: object
 *                                  description: return list user
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 */
routes.get('/list_user', [AuthJsonWebToken.verifyToken], userController.getListUsers);

/**
 * @swagger
 * /user_management/user_manage/sign_up :
 *  post:
 *      summary: create a new user account
 *      requestBody:
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              nickname:
 *                                  type: string
 *                              email:
 *                                  type: string
 *                              password:
 *                                  type: string
 *      responses:
 *          200:
 *              description: returns a json if saved
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *                              jwt?:
 *                                  type: string
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 */
routes.post('/sign_up', userController.signUp);

export default routes;