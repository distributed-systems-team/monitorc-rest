/**
 * POJO object representation of the user into database
 */
export interface User {
    id: number,
    nickname: string,
    email: string,
    password: string
}