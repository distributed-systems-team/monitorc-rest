/**
 * Import Components
 */
import { QueryResult } from 'pg'
import { Connection } from "../../../config/database/connection";
import { User } from "./user";


/**
 * @class model's user
 */
export class UserModel {

    /**
     * Attributes
     */
    public connection: Connection;

    /**
     * @public
     * @constructor
     * Obtain an instance of connection to database
     */
    public constructor() {
        // obtiene la instancia a traves de singleton
        this.connection = Connection.getInstance();
    }

    /**
     * Add new user
     * @param user : info user save to database
     * @returns true if added successfully else returns false
     */
    public create = async (user: User): Promise<number> => {
        try {
            let newUser: QueryResult = await this.connection.executeQuerySQL(
                `INSERT INTO \"user\" (nickname, email, password) VALUES ('${user.nickname}', '${user.email}', '${user.password}') returning id;`
            );
            return newUser.rows[0].id;
        } catch (error) {
            console.error("Error in UserModel create() method: ", error);
            return -1;
        }
    }

    /**
     * remove trusted person
     * @param user : CI possible victim and CI trusted person
     * @returns true if remove successfully else returns false
     */
    public findAll = async (): Promise<Array<User>> => {
        try {
            let users = await this.connection.executeQuerySQL(
                `SELECT * FROM \"user\"`);
            return users.rows;
        } catch (error) {
            console.error("Error in UserModel findAll(): ", error);
            return [];
        }
    }

    /**
     * @public
     * find a user by profile
     * @param id : identifier of user
     * @returns user data else undefined
     */
    public findById = async (id: number): Promise<User | undefined> => {
        try {
            let user = await this.connection.executeQuerySQL(
                `SELECT * FROM \"user\" WHERE id=${id};`);
            return user.rows[0];
        } catch (error) {
            console.error("Error in UserModel findById(): ", error);
            return undefined;
        }
    }

    /**
     * @public
     * find a user by email
     * @param email : email of user
     * @returns user data else undefined
     */
    public findByEmail = async (email: string): Promise<User | undefined> => {
        try {
            let user = await this.connection.executeQuerySQL(
                `SELECT * FROM \"user\" WHERE email='${email}';`);
            return user.rows[0];
        } catch (error) {
            console.error("Error in UserModel findByEmail(): ", error);
            return undefined;
        }
    }
}