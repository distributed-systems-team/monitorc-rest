/**
 * Import Components
 */
import { AuthJsonWebToken } from '../../../middlewares/auth';
import { Encrypt } from '../../../services/encrypt';
import { User } from '../model/user';

/**
 * Import Models
 */
import { UserModel } from '../model/user.model'

/**
 * @class of business logic implementation for manage services users.
 */
export class UserService {

    private userModel: UserModel;

    /**
     * @public
     * @constructor
     * initializes the instances of the models dependent on this service.
     */
    public constructor() {
        this.userModel = new UserModel();
    }

    /**
     * This method does the verification of the login.
     * @param email : email account to verify
     * @param password : password account to verify
     * @returns result if accept session and return JWT but if denied session return message error
     * @catch return an error 500 http to have an error to verify
     */
    public authSessionUser = async (email: string, password: string): Promise<any> => {
        try {
            let userAccount: User | undefined = await this.userModel.findByEmail(email);
            if (userAccount) {
                if (await Encrypt.comparePassword(password, userAccount.password)) {
                    let jwtSession: string = AuthJsonWebToken.generateToken((userAccount.id).toString());
                    return {
                        status: 200,
                        msg: "session success",
                        jwt: jwtSession
                    };
                } else {
                    return {
                        status: 401,
                        msg: "password incorrect"
                    };
                }
            } else {
                return {
                    status: 401,
                    msg: "account not found"
                };
            }
        } catch (error) {
            console.log("Error in user service authSessionUserUser()", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }

    /**
     * Register new account for user
     * @param newUser : user data to save
     * @returns json response http with jwt if success save
     */
    public registerNewUserAccount = async (newUser: User): Promise<any> => {
        try {
            newUser.password = await Encrypt.encryptPassword(newUser.password);
            // falta verificar datos del usuario
            if (!this.userModel.findByEmail(newUser.email)) {
                return {
                    status: 401,
                    msg: "account exists"
                }
            }
            newUser.id = await this.userModel.create(newUser);
            if (newUser.id != 1) {
                let jwtSession: string = AuthJsonWebToken.generateToken(newUser.id.toString());
                return {
                    status: 201,
                    msg: "new account saved successfully",
                    jwt: jwtSession
                }
            } else {
                return {
                    status: 500,
                    msg: "i have problems to register new account"
                };
            }
        } catch (error) {
            console.log("Error in possible user service -> create()", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }

    /**
     * get list user
     * @returns list user info
     */
    public getListUserAccount = async () => {
        let listUsers = await this.userModel.findAll();
        return {
            status: 200,
            msg: "get list success",
            data: listUsers
        }
    }
}