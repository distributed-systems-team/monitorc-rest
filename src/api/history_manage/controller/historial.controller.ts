/**
 * Import Dependencies
 */

/**
 * Import Services
 */
import { Request, Response } from 'express';
import { HistorialService } from '../service/historial.service';

/**
 * @class controller historial, for the implementation of HTTP requests
 */
export class HistorialController {

    /**
     * Attributes
     */
    private historialService: HistorialService;

    /**
     * @constructor
     * Initialize user service for implement business logic
     */
    public constructor() {
        this.historialService = new HistorialService();
    }

    /**
     * save information about device status
     * @param temperature : device's temperature
     * @param humidity : device's humidity
     * @returns info response
     */
    public findByDevice = async (requests: Request, response: Response): Promise<Response> => {
        let { device_id, page, rows } = requests.params;
        let responseHistorial: any = await this.historialService.findByDevice(
            device_id, parseInt(page), parseInt(rows));
        return response.status(responseHistorial.status).json(responseHistorial);
    }

    /**
     * find device history by time range
     * @param request : http request
     * @param response : http response
     * @returns info response
     */
    public findDeviceHistoryByTimeRange = async (requests: Request, response: Response): Promise<Response> => {
        let { device_id, init_date, end_date} = requests.params;
        let responseHistorial: any = await this.historialService.findDeviceHistoryByTimeRange(device_id,init_date, end_date);
        return response.status(responseHistorial.status).json(responseHistorial);
    }

    /**
     * find device historial from a init date
     * @param request : http request
     * @param response : http response
     * @returns info response
     */
    public findDeviceHistoryFromInitDate = async (requests: Request, response: Response): Promise<Response> => {
        let { device_id, init_date} = requests.params;
        let responseHistorial: any = await this.historialService.findDeviceHistoryFromInitDate(device_id,init_date);
        return response.status(responseHistorial.status).json(responseHistorial);
    }
}