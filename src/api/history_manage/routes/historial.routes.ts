/**
 * Import Libraries
 */
import { Request, Response, Router } from 'express';

/**
 * Import Files
 */
import { AuthJsonWebToken } from '../../../middlewares/auth';

/**
 * Import Controllers
 */
import { HistorialController } from '../controller/historial.controller';

// initialize Router
const routes = Router();
// initialize controllers
const historialController: HistorialController = new HistorialController();

// routes http here

/**
 * @swagger
 * /historial_management/historial_manage/list_historial/:device_id/:page/:rows :
 *  post:
 *      summary: This performs the process to get list historial device status
 *      requestBody:
 *              content:
 *                  application/json:
 *      responses:
 *          200:
 *              description: the login verification returns
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              state:
 *                                  type: number
 *                              message:
 *                                  type: string
 *                              data:
 *                                  type: object
 *                                  description: return list user
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 */
routes.get('/list_historial/:device_id/:page/:rows', historialController.findByDevice);

/**
 * @swagger
 * /historial_management/historial_manage/historial_by_time_range/:device_id/:init_date/:end_date :
 *  get:
 *      summary: This finds device history by time range
 *      requestBody:
 *              content:
 *                  application/json:
 *      responses:
 *          200:
 *              description: the device history by time range returns
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              status:
 *                                  type: number
 *                              message:
 *                                  type: string
 *                              data:
 *                                  type: object
 *                                  description: return list history
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 */
routes.get('/historial_by_time_range/:device_id/:init_date/:end_date', historialController.findDeviceHistoryByTimeRange);

/**
 * @swagger
 * /historial_management/historial_manage/historial_from_initdate/:device_id/:init_date :
 *  get:
 *      summary: This finds device historial from a init date
 *      requestBody:
 *              content:
 *                  application/json:
 *      responses:
 *          200:
 *              description: the device historial from a init date returns
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              status:
 *                                  type: number
 *                              message:
 *                                  type: string
 *                              data:
 *                                  type: object
 *                                  description: return list history
 *          500:
 *              description: if this process failed
 *              content:
 *                  application/json:
 *                      type: object
 */
routes.get('/historial_from_initdate/:device_id/:init_date', historialController.findDeviceHistoryFromInitDate);

export default routes;