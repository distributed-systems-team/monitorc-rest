export interface Historial {
    id: number,
    device_id: string,
    temperature: string,
    humidity: string,
    time: string,
    id_rule_brooked: number
};

export interface HistorySummary {
    quantity: number,
    temperature: string,
    time_interval: string,
    quantity_rule_brooked: number
};