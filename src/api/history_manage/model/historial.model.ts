/**
 * Import Components
 */
import { Connection } from "../../../config/database/connection";
import { Historial, HistorySummary } from "./historial";


/**
 * @class model's historial
 */
export class HistorialModel {

    /**
     * Attributes
     */
    public connection: Connection;

    /**
     * @public
     * @constructor
     * Obtain an instance of connection to database
     */
    public constructor() {
        // obtiene la instancia a traves de singleton
        this.connection = Connection.getInstance();
    }

    /**
     * @public
     * find all historial by device
     * @param device : identifier of device for historial
     * @returns historial data else undefined
     */
    public findByDevice = async (deviceId: string, page: number, rows: number): Promise<Array<Historial>> => {
        try {
            let historial = await this.connection.executeQuerySQL(
                `SELECT id, device_id, temperature, humidity, to_char("time",'Mon dy DD YYYY HH24:MI:SS') as "time", get_brock_rule(temperature) as id_rule_brooked
                FROM historial
                WHERE device_id='${deviceId}'
                ORDER BY id DESC
                LIMIT ${rows}
                OFFSET ${page}*${rows};`);
            return historial.rows;
        } catch (error) {
            console.error("Error in historialModel findByDevice(): ", error);
            return [];
        }
    }

    /**
     * @public
     * get all device history by time range
     * @param deviceId identifier of device for historial
     * @param initDate init time range
     * @param endDate end time range
     * @param timeInterval time interval in seconds
     * @returns historial data else an empty array
     */
    public getDeviceHistoryByTimeRange = async (deviceId: string, initDate: string, endDate: string, timeFilter: number): Promise<Array<HistorySummary>> => {
        try {
            let historial = await this.connection.executeQuerySQL(
                `select count(*) quantity, avg(temperature) as temperature,
                to_timestamp(floor((extract('epoch' from "time") / ${timeFilter} )) * ${timeFilter}) 
                at time zone 'UTC' as time_interval, 
                count(get_brock_rule(temperature)) as quantity_rule_brooked
                from historial
                where device_id = '${deviceId}' and 
                time between '${initDate}' and '${endDate}'
                group by time_interval
                order by time_interval;`);
            return historial.rows;
        } catch (error) {
            console.error("Error in historialModel getDeviceHistoryByTimeRange(): ", error);
            return [];
        }
    }

    /**
     * @public
     * get device historial from a init date
     * @param deviceId identifier of device for historial
     * @param initDate init time range
     * @returns historial data else an empty array
     */
    public getDeviceHistoryFromInitDate = async (deviceId: string, initDate: string): Promise<Array<Historial>> => {
        try {
            let historial = await this.connection.executeQuerySQL(
                `select h.*, get_brock_rule(temperature) as id_rule_brooked
                from historial h
                where device_id = '${deviceId}' and "time" >= '${initDate}'
                order by "time";`);
            return historial.rows;
        } catch (error) {
            console.error("Error in historialModel getDeviceHistoryFromInitDate(): ", error);
            return [];
        }
    }
}