/**
 * Import Models
 */

import { Historial, HistorySummary } from "../model/historial";
import { HistorialModel } from "../model/historial.model";

import { DateService} from "../../../services/date_service";


/**
 * @class of business logic implementation for manage services users.
 */
export class HistorialService {

    historialModel: HistorialModel;

    /**
     * @public
     * @constructor
     * initializes the instances of the models dependent on this service.
     */
    public constructor() {
        this.historialModel = new HistorialModel();
    }

    /**
     * get list historial saved state device's information
     * @returns list historial saved
     */
    public findByDevice = async (deviceId: string, page: number, rows: number): Promise<any> => {
        try {
            let listHistorial: Array<Historial> = await this.historialModel.findByDevice(deviceId, page, rows);
            return {
                status: 200,
                msg: "get list success",
                data: listHistorial
            }
        } catch (error) {
            console.log("Error in HistorialService findByDevice()", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }

    /**
     * find device history by time range
     * @param deviceId : the device identifier
     * @param initDate : range start date
     * @param endDate : range end date
     * @returns list historial
     */
    public findDeviceHistoryByTimeRange = async (deviceId: string, initDate: string, endDate: string): Promise<any> => {
        try {
            let quantityDays: number = DateService.getDaysDiff(new Date(initDate), new Date(endDate));
            let timeFilter = 600; // a day filter
            if (quantityDays >= 2 && quantityDays <= 7) { 
                timeFilter = 7200; // a week filter
            }else if(quantityDays >= 8 && quantityDays <=31){ 
                timeFilter = 28800; // a month filter
            }
            let listHistorial: Array<HistorySummary> = await this.historialModel.getDeviceHistoryByTimeRange(deviceId, initDate, endDate, timeFilter);
            for (const historialSummary of listHistorial) {
                historialSummary.time_interval = DateService.toLocaleDatetime(new Date(historialSummary.time_interval));
            }
            return {
                status: 200,
                msg: "get list success",
                data: listHistorial
            }
        } catch (error) {
            console.log("Error in HistorialService findDeviceHistoryByTimeRange()", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }

    /**
     * find device historial from a init date
     * @param deviceId : the device identifier
     * @param initDate : range start date
     * @returns list historial
     */
    public findDeviceHistoryFromInitDate = async (deviceId: string, initDate: string): Promise<any> => {
        try {
            let listHistorial: Array<Historial> = await this.historialModel.getDeviceHistoryFromInitDate(deviceId, initDate);
            for (const historial of listHistorial) {
                historial.time = DateService.toLocaleDatetime(new Date(historial.time));
            }
            return {
                status: 200,
                msg: "get list success",
                data: listHistorial
            }
        } catch (error) {
            console.log("Error in HistorialService findDeviceHistoryFromInitDate()", error);
            return {
                status: 500,
                msg: "internal server error"
            };
        }
    }
}