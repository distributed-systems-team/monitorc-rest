/**
 * @ignore
 */
export const options = {
    definition: {
        openapi: "3.0.3",
        info: {
            title: "Monitor°C Rest API",
            version: "1.0.0",
            description: "Rest API for 'Monitor°C' software"
        },
        servers: [
            {
                url: "http://localhost:3000"
            }
        ]
    },
    apis: [
        "./src/api/**/**/routes/*.ts",
        "./src/*.ts"
    ]
}