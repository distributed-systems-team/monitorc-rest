/**
 * Import Dependencies
 */
import bcryptjs from 'bcryptjs';

/**
 * @class static class for password verification
 * and encryption processes
 */
export class Encrypt {
    /**
     * @public
     * Encrypt password in Salt, encryption cycles are used, which are 12 cycles
     * @param password : password to encrypt
     * @returns password encrypted out
     */
    public static encryptPassword = async (password: string): Promise<string> => {
        const salt = await bcryptjs.genSalt(12)
        return await bcryptjs.hash(password, salt);
    }

    /**
     * compares the password and the other one already encrypted for
     * verification if it is correct
     * @param password : password saved
     * @param passwordCompare : password to verify
     * @returns result if the password is correct, or if it is not.
     * @throws this params is not validated or null
     */
    public static comparePassword = async (password: string, passwordCompare: string): Promise<boolean> => {
        try {
            return await bcryptjs.compare(password, passwordCompare);
        } catch (error) {
            throw new Error(`Error to password compare ${error}`);
        }
    }
}