/**
 * @class static class for date management
 */
export class DateService{
    private static ONE_SECOND = 1000;
    private static ONE_MINUTE = 60;
    private static ONE_HOUR = 60;
    private static ONE_DAY = 24;
    /**
     * @public
     * calculate the difference in days between two dates
     * @param firstDate : date one
     * @param secondDate : date two
     * @returns number days between two dates
     */
    public static getDaysDiff = (firstDate: Date, secondDate: Date): number => {
        const msBetweenDates:number = secondDate.getTime() - firstDate.getTime();
        return (msBetweenDates) / (this.ONE_SECOND * this.ONE_MINUTE * this.ONE_HOUR * this.ONE_DAY);
    }

    public static toLocaleDatetime = (date: Date): string =>{
        let month = (date.getMonth() + 1)<10? "0"+(date.getMonth() + 1): (date.getMonth() + 1);
        let day = date.getDate()<10? "0"+date.getDate():date.getDate();
        let hour = date.getHours()<10? "0"+date.getHours(): date.getHours();
        let minute = date.getMinutes()<10? "0"+date.getMinutes(): date.getMinutes();
        let second = date.getSeconds()<10? "0"+date.getSeconds(): date.getSeconds();
        return date.getFullYear() + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
    }
}