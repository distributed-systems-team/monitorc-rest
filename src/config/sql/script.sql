--CREATE ROLE uyunicode with LOGIN CONNECTION LIMIT -1 PASSWORD 'uyunicode.bo';
--CREATE DATABASE monitorc with owner=uyunicode encoding='UTF8' tablespace=pg_default CONNECTION LIMIT=-1;

create table "user" (
    id serial primary key not null,
    nickname varchar(100) not null,
    email varchar(100) not null,
    password text not null
);

insert into "user"(nickname, email, password) values
('@ruddy','ruddyq18@gmail.com','123456'),
('@ruddy_quispe','ruddyq.bryan.ts@gmail.com','123456'),
('@zuleny','zuleny.cr@gmail.com','123456');

create table device(
   id_device text not null primary key,
   date_registered timestamp  not null,
   temperature decimal(5,3) null,
   humidity decimal(5,3) null,
   last_session timestamp null
);

insert into device(id_device, date_registered, temperature, humidity, last_session) values
('32:AE:A4:07:0D:66',now(),null,null,null);

create table historial(
  id serial primary key not null,
  device_id text not null,
  temperature decimal(5,3) not null,
  humidity decimal(5,3) not null,
  time timestamp not null,
  foreign key(device_id) references device(id_device)
  on update cascade on delete cascade
);

create table "rule"(
   id serial primary key not null,
   min_temperature decimal(6,3) not null,
   max_temperature decimal(6,3) not null,
   message varchar(256) not null,
   emails varchar(100)[] not null
);

insert into "rule"(min_temperature, max_temperature, message, emails) values
(20,25,'mensaje de alerta, se excede la temperatura hasta lo minimo de 20°C - 25°C',
'{"ruddyq18@gmail.com","ruddy.bryan.ts@gmail.com","zuleny.cr@gmail.com"}');

-- update info into device
create or replace function update_last_info_device()returns trigger as 
$BODY$
begin
	update device set temperature=new.temperature,
	humidity=new.humidity, last_session=now() where id_device=new.device_id;
	return new;
end;
$BODY$
language 'plpgsql';

-- trigger to update device after insert into historial
create or replace trigger last_info_on_insert_historial after insert
on historial
for each row
	execute procedure update_last_info_device();

-- insert new device when don't registered lastest (trigger before insert historial)
create or replace function identify_new_device()returns trigger as
$BODY$
declare exists_device integer;
begin
	exists_device:=(select count(*) from device where id_device=new.device_id);
	if (exists_device=0) then
		insert into device(id_device, date_registered, temperature, humidity, last_session) values 
		(new.device_id,now(),new.temperature,new.humidity,now());
	end if;
	return new;
end
$BODY$
language 'plpgsql';

-- trigger to insert new device before insert into historial
create or replace trigger create_new_device before insert
on historial
for each row
	execute procedure identify_new_device();

-- get rule brocked by historial temperature
create or replace function get_brock_rule(temperature decimal(6,3))returns integer as 
$BODY$
begin
	 return (select id from "rule" where temperature between min_temperature and max_temperature);
end
$BODY$
language 'plpgsql';